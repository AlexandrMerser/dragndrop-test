import React, {useEffect, useState} from 'react';
import {DragDropContext, Droppable, Draggable, DropResult} from "react-beautiful-dnd";

const itemsFromBackend = [
    {
        id: "1", content: "1"
    },
    {
        id: "2", content: "2"
    },
    {
        id: "3", content: "3"
    },
    {
        id: "4", content: "4"
    },
];

const itemsFromBackend2 = [
    {
        id: 'fdsf',
        content: 'hi my'
    },
    {
        id: 'fdsfdsff',
        content: 'hi dfsdf3 342 '
    }
]

interface columnsFromBackendInterface {
    [key: string]: {
        name: string;
        items: {
            id: string;
            content: string;
        }[];
    }
}


const columnsFromBackend: columnsFromBackendInterface =
    {
        'd1d2d3': {
            name: "Todo",
            items: itemsFromBackend
        },
        'dasdasd2': {
            name: "add",
            items: itemsFromBackend2
        }
    };


const onDragEnd = (
    result: DropResult,
    columns: columnsFromBackendInterface,
    setColumns: (val: columnsFromBackendInterface) => void
) => {
    const {source, destination} = result;
    if (!destination) return;

    if (destination.droppableId !== source.droppableId) {
        const sourceColumn = columns[source.droppableId];
        const destinationColumn = columns[destination.droppableId];

        const sourceItems = [...sourceColumn.items];
        const destinationItems = [...destinationColumn.items];

        const [removed] = sourceItems.splice(source.index, 1);
        destinationItems.splice(destination.index, 0, removed);
        setColumns({
            ...columns,
            [destination.droppableId]: {
                ...sourceColumn,
                items: destinationItems
            },
            [source.droppableId]: {
                ...destinationColumn,
                items: sourceItems
            }
        });
        return;
    }

    const column = columns[source.droppableId];
    let copiedItems = [...column.items];

    const [removed] = copiedItems.splice(source.index, 1);
    copiedItems.splice(destination.index, 0, removed);

    setColumns({
        ...columns,
        [source.droppableId]: {
            ...column,
            items: copiedItems
        }
    })
};

function App() {

    const [columns, setColumns] = useState<columnsFromBackendInterface>(columnsFromBackend);

    useEffect(() => {
        // change event
        console.log('change columns', columns);
    }, [columns]);

    return (
        <div style={{
            display: 'flex',
            justifyContent: 'center',
            height: '100%'
        }}>
            <DragDropContext
                onDragEnd={result => onDragEnd(result, columns, setColumns)}
            >
                <>
                    {
                        Object.entries(columns).map(([id, column]) => {
                            return (
                                <div style={{
                                    margin: '0 25px'
                                }}
                                     key={id}
                                >
                                    <Droppable droppableId={id}>
                                        {
                                            (provided, snapshot) => {
                                                return (
                                                    <div
                                                        {...provided.droppableProps}
                                                        ref={provided.innerRef}
                                                        style={{
                                                            background: snapshot.isDraggingOver ? 'gray' : 'blue',
                                                            padding: 4,
                                                            width: 250,
                                                            minHeight: 500,
                                                            height: 500,
                                                            overflowY: 'auto'
                                                        }}
                                                    >
                                                        {
                                                            column.items.map((item, index) => {
                                                                return (
                                                                    <Draggable draggableId={item.id} index={index}
                                                                               key={item.id}>
                                                                        {(provided1, snapshot1) => {
                                                                            return <div
                                                                                ref={provided1.innerRef}
                                                                                {...provided1.draggableProps}
                                                                                {...provided1.dragHandleProps}
                                                                                style={{
                                                                                    userSelect: "none",
                                                                                    width: '100%',
                                                                                    height: 50,
                                                                                    background: snapshot1.isDragging ? 'white' : 'yellow',
                                                                                    margin: '5px 0',
                                                                                    ...provided1.draggableProps.style
                                                                                }}
                                                                            >
                                                                                {item.content}
                                                                            </div>
                                                                        }}
                                                                    </Draggable>
                                                                )
                                                            })
                                                        }
                                                        {provided.placeholder}
                                                    </div>
                                                )
                                            }
                                        }
                                    </Droppable>
                                </div>
                            )
                        })
                    }
                </>
            </DragDropContext>
        </div>
    );
}

export default App;
